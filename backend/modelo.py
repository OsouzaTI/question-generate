from transformers import AutoTokenizer, AutoModelForCausalLM

tokenizer = AutoTokenizer.from_pretrained("osouza/gemma-portuguese-luana-2b-questoes-beecrowd")
model = AutoModelForCausalLM.from_pretrained("osouza/gemma-portuguese-luana-2b-questoes-beecrowd")

def categorizar_dificuldade(valor):
    if 1 <= valor <= 2:
        return "Muito Simples"
    elif 3 <= valor <= 4:
        return "Simples"
    elif 5 <= valor <= 6:
        return "Médio"
    elif 7 <= valor <= 8:
        return "Difícil"
    elif 9 <= valor <= 10:
        return "Muito Difícil"
    else:
        return "Valor fora do intervalo"


def get_completion(query: str, max_length=500) -> str:

  prompt_template = """
  <prompt>Gere uma questão de dificuldade {query}</prompt>
  """

  prompt = prompt_template.format(query=query)
  encodeds = tokenizer(prompt, return_tensors="pt", add_special_tokens=True)
  model_inputs = encodeds.to(device)


  generated_ids = model.generate(**model_inputs, max_new_tokens=max_length, do_sample=True, pad_token_id=tokenizer.eos_token_id)
  decoded = tokenizer.decode(generated_ids[0], skip_special_tokens=True)

  return (decoded)

