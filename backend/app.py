from flask import Flask
from flask_cors import cross_origin

from modelo import get_completion

app = Flask(__name__)

@app.route('/')
@cross_origin()
def generate_question():
    return {
        'question': 'Questão teste...'
    }
    # return ('', 404)